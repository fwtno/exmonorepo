defmodule ExMonorepo.MixProject do
  use Mix.Project

  def project do
    version = "0.2.1"

    [
      app: :exmonorepo,
      version: version,
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      docs: docs(version),
      # Hex config
      name: "exmonorepo",
      package: package(),
      description: description(),
      source_url: "https://gitlab.com/fwtno/exmonorepo"
    ]
  end

  def application do
    [extra_applications: [:logger, :crypto]]
  end

  defp deps do
    [{:ex_doc, "~> 0.30", only: :dev, runtime: false}]
  end

  defp docs(version) do
    [
      extras: [
        "README.md",
        "LICENSE",
        "CHANGELOG.md",
        "docs/guides/converting-umbrella-project.md"
      ],
      main: "readme",
      source_ref: "v#{version}"
    ]
  end

  def description() do
    """
    Scan local directory for mix files and use a directory as a monorepo.
    """
  end

  def package() do
    [
      licenses: ["BSD-3-Clause"],
      links: %{"Gitlab" => "https://gitlab.com/fwtno/exmonorepo"}
    ]
  end
end
