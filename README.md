# ExMonorepo

Add minimal support for storing apps as a unstructured monorepo.

## Installation

### From hex

```
$ mix archive.install hex exmonorepo
```


### From Source
```
$ git clone git://gitlab.com/fwtno/exmonorepo
$ cd exmonorepo && mix archive.build && mix archive.install exmonorepo*.ez
```


## Using

To use it make a file `monorepo.exs` with the contents `repo = "./"` and set `use ExMonorepo` in `mix.exs`.

```elixir
defmodule ExpkgTest.MixProject do
  use Mix.Project

  # Use monorepo implementation
  use ExMonorepo

  def project do
    [
      app: :expkg,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
    ]
  end

  def application do
    [extra_applications: [:logger]]
  end

  defp deps do
    [{:dependency, "~> 0.1"}, {:root, "~> 0.1"}]
  end
end
```



## Known Limitations

 * Only supports mix dependencies - not rebar/make/...
 * Umbrella apps are not tested - may behave weird
 * Consider this as alpha software, paper cuts and chopped limbs will occur.
 * Keeping `__DIR__` as monorepo will have weird side effects.
 * This is a gready SCM, it will disable fetching from hex (and possibly others)
   in case it find the dependency. It's important to keep `deps/` folder
   out of search path.
