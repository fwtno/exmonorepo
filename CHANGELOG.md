# Changelog

## exmonorepo-0.2.1 (2023-09-17)

Generalize configuration through `monorepo.exs`, adds task `monorepo.changes`
and `monorepo.compile`. 

New monorepos should use `repo = "./repo"` in `monorepo.exs` at root level to
establish repository configuration. For now `monorepo.compile` can be used
for a only compiling changed applications in the repo but regular `compile`
should work fine.

There's no significant testing done at this point but two moderately complex
umbrella projects have been converted without any difficulties.

## exmonorepo-0.2.0 (2023-09-06)

Moves monorepo functions into own file and add function to export all dependency as archives.

