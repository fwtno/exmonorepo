defmodule ExMonorepo.SCM do
  @behaviour Mix.SCM
  @moduledoc false

  @impl true
  def fetchable? do
    # In case of a fetchable dependency
    # `mix deps.loadpaths` automatically compiles any non fetchable dependency
    # which  `Mix.Dep.ok?/1` returns true for and it's not fetchable. This causes
    # increased compile time when there are many apps in monorepo.
    true
  end

  @impl true
  def format(opts) do
    repo = get_repo_config()

    if opts[:archive] do
      "archive: #{Path.relative_to(opts[:dest], repo.dist)}"
    else
      "repo: #{Path.relative_to(opts[:dest], repo.repo)}"
    end
  end

  @impl true
  def format_lock(_opts) do
    nil
  end

  defp cached_get(key, generator) do
    try do
      :persistent_term.get(key)
    rescue
      ArgumentError ->
        res = generator.()
        :ok = :persistent_term.put(key, res)
        res
    end
  end

  @impl true
  def accepts_options(app, opts) do
    cached_get({__MODULE__, app}, fn ->
      # @deprecated v0.2.1 - don't use `monorepo` and `monodist`, instead
      #             use `exmonorepo: [repo: ...]` or `repo.exs` file
      repo = get_repo_config()

      # lock comes as a tuple or nil
      lock = Map.get(Mix.Dep.Lock.read(), app, nil)

      {_app, vsn, chksum} = Mix.Tasks.Monorepo.Export.lock_meta(app, lock)

      archive =
        Path.join([
          repo.dist,
          "#{Mix.env()}",
          Mix.Tasks.Monorepo.Export.target(),
          "#{app}-#{vsn || "0.0.0"}-#{chksum}.ez"
        ])

      # place all the files in same locaiton as
      srcdir = Path.dirname(repo.source)
      {build_path, repo} = Map.pop(repo, :build_path, Path.expand("_build", srcdir))
      {config_path, repo} = Map.pop(repo, :config_path, Path.expand("config/config.exs", srcdir))
      {deps_path, repo} = Map.pop(repo, :deps_path, Path.expand("deps", srcdir))
      {lock_file, repo} = Map.pop(repo, :lock_file, Path.expand("mix.lock", srcdir))

      scanned = maybe_scan(repo.repo, [deps_path, build_path])

      cond do
        dep_path = scanned[app] ->
          IO.puts(" => using #{app} from repo=#{Path.relative_to_cwd(srcdir)}")
          # if the app is found in repo use it.
          Keyword.merge(opts,
            # env is required, otherwise any child dependencies using `only: _` will fail
            env: Mix.env(),
            # pass on the monorepo config
            exmonorepo: repo,
            dest: Path.expand(dep_path),
            path: Path.expand(dep_path, __DIR__),
            in_umbrella: false,
            build_path: build_path,
            config_path: config_path,
            deps_path: deps_path,
            lockfile: lock_file
          )

        File.exists?(archive) ->
          IO.puts(" => using #{app} from dist (#{Path.relative_to_cwd(archive)})")

          # File matches lock file and env target; use it instead of recompiling
          # and redownloading
          Mix.Local.check_elixir_version_in_ebin(archive)

          # Append the archive to path and disable compilation. This ensures code is
          # immediately available for use by other dependencies. Erlang's `erl_prim_loader`
          # treats the archive as a normal directory and we can access it directly.
          Code.append_path(Path.join([archive, Path.basename(archive, ".ez"), "ebin"]))

          Keyword.merge(opts,
            # env is required, otherwise any child dependencies using `only: _` will fail
            env: Mix.env(),
            # pass on the monorepo config
            exmonorepo: repo,
            dest: Path.expand(archive),
            archive: true,
            compile: false,
            build_path: build_path,
            config_path: config_path,
            deps_path: deps_path,
            lockfile: lock_file
          )

        true ->
          nil
      end
    end)
  end

  @impl true
  def checked_out?(opts) do
    if opts[:archive] do
      File.regular?(opts[:dest])
    else
      File.dir?(opts[:dest])
    end
  end

  @impl true
  def lock_status(_opts) do
    :ok
  end

  @impl true
  def equal?(opts1, opts2) do
    opts1[:dest] == opts2[:dest]
  end

  @impl true
  def managers(_opts) do
    []
  end

  @impl true
  def checkout(opts) do
    path = Path.relative_to_cwd(opts[:dest])
    Mix.raise("Cannot checkout local repo dependency, expected a dependency at #{path}")
  end

  @impl true
  def update(opts) do
    # TODO when dependency is cached (ie an archive) the update will never succeed
    opts[:lock]
  end

  @doc """
  Return the cached results of monorepo scan
  """
  def scan(project \\ Mix.Project.config()) do
    try do
      :persistent_term.get(__MODULE__)
    rescue
      ArgumentError ->
        maybe_scan(project[:exmonorepo][:repo], [project[:build_path], project[:deps_path]])
    end
  end

  defp get_repo_config() do
    {_sm, %{config: config}} = Mix.ProjectStack.top_and_bottom()
    get_repo_config(config)
  end

  defp get_repo_config(config) do
    monocfg = Keyword.merge(config[:exmonorepo] || [], :persistent_term.get(ExMonorepo, []))

    # @deprecated v0.2.1 - don't use `monorepo` and `monodist`, instead
    #             use `exmonorepo: [repo: ...]` or `repo.exs` file
    legacy =
      %{
        repo: config[:monorepo],
        # in case a dist directory is given archives may be fetched from there
        # instead of going through compilation cycle
        dist: config[:monodist] || "./dist"
      }

    Map.merge(legacy, Map.new(monocfg))
  end

  defp descendant(child, matches) do
    Enum.any?(matches, fn parent ->
      # Split to list as String.starts_with would give false positives
      nil != parent && List.starts_with?(Path.split(child), Path.split(parent))
    end)
  end

  defp maybe_scan(repo, exclude) do
    case :persistent_term.get(__MODULE__, nil) do
      # Discover repo one
      nil when repo != nil ->
        search_path = Path.join(repo, "**/mix.exs")

        apps =
          for path <- Path.wildcard(search_path), not descendant(path, exclude), into: %{} do
            # Get the absolute path now so that path is not incorrectly
            # joined later.
            path = Path.expand(path)
            dir = Path.dirname(path)
            # Assume dir name to be the app name
            app = String.to_atom(Path.basename(dir))

            {app, dir}
          end

        :persistent_term.put(__MODULE__, apps)

        apps

      nil ->
        # no top level repo configured. nothing will be matched
        %{}

      scanned ->
        scanned
    end
  end
end
